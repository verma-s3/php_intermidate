# Intermediate PHP - Day 2 Topics

* Create Intermediate PHP repsitory
    - share with me READ only
    - create public folder
    - create day2 folder inside public

* String functions
    - trim
    - rtrim
    - ltrim
    - str_replace
    - strip_tags

* Regular Expressions
    - preg_match
    - preg_replace
    - assertions

* Classes (basics)
    - classes and objects
    - inheritance

* Assignment 1
    - Validator class
    - Decoupled

