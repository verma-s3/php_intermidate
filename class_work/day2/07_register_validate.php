<?php

require __DIR__ . '/../config.php';
require __DIR__ . '/../classes/Validator.php';

$v = new Validator;

$title = 'Register';

// test for POST
if("POST" == $_SERVER['REQUEST_METHOD']) {

    // validate fields
    $v->required('phone');
    $v->required('name');
    $v->required('email');

    $v->phone('phone');


    // if no errors
    if(count ($v->errors() ) === 0){
        var_dump('SUCCESS');
        die;
    }
    // end if no errors

} // end test for POST

$errors = $v->errors();

?><!DOCTYPE html>
<html>
<head>
    <title><?=esc($title)?></title>
</head>
<body>

    <h1><?=esc($title)?></h1>

    <?php include __DIR__ . '/../errors.inc.php'; ?>

    <form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
        <p><label for="name">Name</label><br />
        <input type="text" name="name" value="<?=clean('name')?>" /></p>
        <p><label for="email">Email</label><br />
        <input type="text" name="email" value="<?=clean('email')?>" /></p>
        <p><label for="phone">Phone</label><br />
        <input type="text" name="phone" value="<?=clean('phone')?>" /></p>
        <p><button type="submit">Submit</button></p>
    </form>

</body>
</html>