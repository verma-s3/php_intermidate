<?php

// Object Oriented PHP

// Javascript
// JS has prototypal inheritance

// PHP
// PHP has classical inheritance
    // inheritance
    // interfaces
    // abstract classes

// What is a class?
// A class is a blueprint for creating objects
// We can create many objects from the same class

// Normally, in PHP, each class is stored in its own file, 
// with a filename that exactly matches the class name

// This class would be stored in a file named User.php
// Uppercase Camelcase (psr-2 style rule)
class User 
{

    /**
     * User's name
     * @var String
     */
    public $name;

    /**
     * User's email
     * @var String
     */
    public $email;

    /**
     * Users's age
     * @var Int
     */
    public $age;

    /**
     * User's password
     * @var String
     */
    private $password;

    /**
     * Set password
     * @param String $password
     */
    public function setPassword($password) 
    {
        $this->password = $password;
    }

    /**
     * Get the user's password
     * @return String password
     */
    public function getPassword()
    {
        return $this->password;
    }

}

$user1 = new User;

var_dump($user1);

// When we assign new properties to an object from outside that
// object, they are by default 'public'... visible everywhere.
$user1->name = 'Franke';
$user1->email = 'franke@example.com';
$user1->age = 12;
// must use public setter method to set value of private property
$user1->setPassword(md5('mypass'));

// In order to access a private property, you must ust
// public setters and getters.

var_dump($user1);

var_dump($user1->getPassword());

// properties (and methods) can be... (Data Hiding)
// public -- visible inside and outside the object
// protected -- visible inside an object, and child object (inherance)
// private -- visible ONLY within the object in which they are declared