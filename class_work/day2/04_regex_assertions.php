<?php

// Regex assertions

// Must have one upper case
// Must have one lower case
// Must have one number
// Must have one special
// Must have at least six characters

$pattern = '';

$password_bad= 'mypass';
$password_good = 'P@ssw0rd!';

// samples from class
// ^[A-Za-z0-9\&\*\&\$\@]{6,}$
// This won't work, because we're not saying the
// string MUST HAVE these things, only that it
// CAN have these things

// Quantifiers in regex
// ? 0 or 1
// + 1 or more
// * 0 or more
// {} range

// We can do this with Regex Assertions
// This means Look Ahead into the string, and assert that something is true
// (?=.*[[:punct:]]+)// special chars... or do them manually

//(?=  )  look ahead
// .*  zero or more of any character
// [A-Z]+  followed by at least one uppercase letter

// (?=.*[A-Z]+) // make sure the password has an uppercase letter
// (?=.*[a-z]+) // make sure the password has a lowercase letter
// (?=.*[0-9]+) // make sure the password has a number
// (?=.*[\!\@\#\$\%\^\&\*\(\)]+) // make sure the password has a special character
// // '/.{6,}/'; // basic password, any characters, six or more

$pattern = '/(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[0-9]+)(?=.*[\!\@\#\$\%\^\&\*\(\)]+).{6,}/';

var_dump(preg_match($pattern, $password_bad, $matches));

var_dump(preg_match($pattern, $password_good, $matches));

