<?php

require_once __DIR__ . '/../config.php';

// useful string functions
// 
// trim
// ltrim
// rtrim (chop)

$string = '     Dave     '; // five spaces on either side

// trim()
// removes spaces (by default) from left and right side of string
// Good function to use on values prior to validating them
// and prior to insert into database
echo '=>' . $string . '<=';
echo "<br />\n";
echo '=>' . trim($string) . '<='; 


// ltrim()
// removes spaces (by default) from left side of string
echo "<br />\n";
echo '=>' . ltrim($string) . '<='; 

// rtrim()
// removes spaces (by default) from right side of string
echo "<br />\n";
echo '=>' . rtrim($string) . '<='; 

// removing other characters

$names = array('Bob', 'Carol', 'Ted', 'Alice');

// loop through names
// concatenate each name onto string
// append a , and space
// To remove the extraneous comma and space, use the rtrim()
// function, and add a second parameter (a string, with the
// characters we want to trim.  In this a comma and a space)

$names_string = '';
foreach($names as $name) {
    $names_string .= $name . ', ';
}

echo "<br />\n";
echo $names_string;
echo "<br />\n";

$names_string = rtrim($names_string, ', ');

echo "<br />\n";
echo $names_string;
echo "<br />\n";

// create array of mock DB results for a user table
// associative array.  Field name as key, and appropriate
// value

$results = array(
    'user_id' => 3,
    'first_name' => 'Dave',
    'last_name' => 'Jones',
    'email_address' => 'dave@example.com',
    'street_address' => '123 Any Street',
    'age' => 22
);

// You don't want to do this (unless you're a sucker for extra work)
echo "<p>
<strong>First Name</strong>: {$results['first_name']}<br />
<strong>Last Name</strong>: {$results['last_name']}<br />
<strong>Email Address</strong>: {$results['email_address']}<br />
<strong>Street Address</strong>: {$results['street_address']}<br />
<strong>Age</strong>: {$results['age']}
</p>";


// We will create a function calle label()
// label() accepts a string
// replace all underscores with a space
// upperacase all words
// return that value

echo "<p>\n";
foreach($results as $key => $value) {
    if($key !== 'user_id') {
        echo "<strong>" . label($key) . "</strong>: $value <br />";
    }
}
echo "</p>\n";

// You can also assign the label to a variable and use it more simply
echo "<p>\n";
foreach($results as $key => $value) {
    if($key !== 'user_id') {
        $label = label($key);
        echo "<strong>$label</strong>: $value <br />";
    }
}
echo "</p>\n";
