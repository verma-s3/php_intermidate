<?php

require_once(__DIR__ . '/../config.php');
require_once __DIR__ . '/../config.php';

// Demonstrating use of strip_tags
// 
$title = "My great blog post";

$content = <<<EOT

<div>

<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, <strong>ultricies</strong> eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

<p>Pellentesque habitant morbi <em>tristique senectus</em> et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

</div>

EOT;


?><!DOCTYPE html>
<html>
<head>
    <title><?=esc($title)?></title>
</head>
<body>

    <h1><?=esc($title)?></h1>

    <?=raw($content)?>

</body>
</html>

