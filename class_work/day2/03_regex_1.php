<?php

// PHP Regex Basics

$pattern = '/cat/';
$string ="I am a cat lover";

// 'cat' -- match
// 'dogcat' -- match

// in PHP we use preg_match

if(preg_match($pattern, $string) === 1){
    echo '<p>We have a match!</p>';
} else {
    echo '<p>No match</p>';
}

// preg_match has 3 potential return values

// 0 -- no match
// 1 -- match
// false -- error

// How do I match exactly 'cat'

$pattern = '/^cat$/';
$pattern2 = '/^cat$'; // will cause error, no closing delimiter
$string1 = 'I am a cat';
$string2 = 'cat';

// Should return 0 -- no match
var_dump(preg_match($pattern, $string1));

// should return 1 -- match
var_dump(preg_match($pattern, $string2));

// should return false -- error in pattern
// var_dump(preg_match($pattern2, $string2));

// Review of regex


// classes of characters

//  /[A-Za-z]/ -- matches single character
//  [A-z] -- Ascii range from A to z
//  [A-Za-z0-9^%&*#@!]{6,} // require minimum of 6

// Demonstrate capture groups

// standard North American Phone number
$pattern = '/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/';

$num1 = '204-222-1738';

// Should return 1... matching number
// create an array in current scope named $matches
// that contains our capture groups
var_dump(preg_match($pattern, $num1, $matches)); // 1

// $matches should be available in scope at this point
var_dump($matches);

// $matches[0] = The full match
// $matches[1] = The first capture group
// $matches[2] = The second capture group
// etc
// etc

// Note... outer groups will appear in array before inner groups
 // Example quiz questions based on the above:
 //

// 1. What is the value of $num in the following snippet
// $pattern = '/^(([0-9]{3})-?([0-9]{3})-?([0-9]{4}))$/';
// $num1 = '204-222-1738';
// preg_match($pattern, $num1, $matches)
// $num = count($matches);

$formatted_num = "({$matches[1]}) {$matches[2]} {$matches[3]}";

var_dump($formatted_num);

