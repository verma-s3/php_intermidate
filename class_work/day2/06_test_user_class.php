<?php

require __DIR__ . '/../classes/User.php';
require __DIR__ . '/../classes/Instructor.php';

$user = new User('David Jones', 'david@example.com', 12, md5('mypass'));
$user2 = new User('Sally Jenkins', 'sally@hotmail.com', 23, md5('mypass'));
$user3 = new User('Rolph McToolie', 'rolph@aol.com', 42, md5('mypass'));

var_dump($user);
var_dump($user2);
var_dump($user3);

if($user == $user2) {
    echo 'Equals';
} else {
    echo 'Not Equals'; // Same Class, but different objects
}

$steve = new Instructor('Steve', 'edu@pagerange.com', 12, md5('mypass'), 'Web Development');

var_dump($steve);

// Polymorphism

if($steve instanceof Instructor) {
    echo '<p>Steve is an instructor</p>';
}

if($steve instanceof User) {
    echo '<p>Steve is User</p>';
}