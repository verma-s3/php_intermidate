<?php

class Validator
{
    /**
     * Array for tracking validation errors
     * @var array
     */
    protected $errors = [];

    /**
     * Trimmed version of $_POST array
     * @var array
     */
    protected $post = [];

    public function __construct()
    {

       foreach($_POST as $key => $value) {
             $this->post[$key] = trim($value);
       }     
    }

    public function required($field)
    {
        if(empty($this->post[$field])) {
            $label = $this->label($field);
            $this->setError($field, "$label is required");
        }
    }

    // create method to validate phpne number. It should accept, 
    // the field name
    public function phone($field)
    {
        $pattern = '/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/';
        if(preg_match($pattern, $this->post[$field]) !== 1) {
            $this->setError($field, 'Please enter a valid phone number');
        }
    }

    /**
     * Get validation errors
     * @return Array
     */
    public function errors()
    {
        return $this->errors;
    }

    protected function setError($field, $message)
    {
        if(empty($this->errors[$field])) {
            $this->errors[$field] = $message;
        }
        
    }

    /**
     * Create a label from a string
     * @param  String $string for example, a db table field name
     * @return String
     */
    protected function label($string) 
    {
        // replace underscores with a space
        // uppercase each word
        // return the result
        return ucwords( str_replace('_', ' ', $string) ); 
    }
}