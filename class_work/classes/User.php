<?php

class User 
{

    /**
     * User's name
     * @var String
     */
    public $name;

    /**
     * User's email
     * @var String
     */
    public $email;

    /**
     * Users's age
     * @var Int
     */
    public $age;

    /**
     * User's password
     * @var String
     */
    private $password;


    public function __construct($name, $email, $age, $password)
    {
        $this->email = $email;
        $this->name = $name;
        $this->age = $age;
        $this->password = $password;
    }

    /**
     * Set password
     * @param String $password
     */
    public function setPassword($password) 
    {
        $this->password = $password;
    }

    /**
     * Get the user's password
     * @return String password
     */
    public function getPassword()
    {
        return $this->password;
    }

} // end class