<?php

class Instructor extends User
{
    
    // Instructor has all of user's public and protected properties
    // Instructor has all of users public and protected methods
    
    /**
     * Program in which instructor teaches
     * @var String
     */
    protected $program;

    public function __construct($name, $email, $age, $password, $program)
    {
        // We can set our own properties with our constructor
        $this->program = $program;

        // But we need to call the parent class's constructor
        // for the others
        parent::__construct($name, $email, $age, $password);
    }

}