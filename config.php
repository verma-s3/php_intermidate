<?php

// set error display and reporting level
// could be set manually inside php.ini
// which would make them global

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// set Database Credentials

define('DB_DSN', 'mysql:host=localhost;dbname=booksite');
define('DB_USER', 'web_user');
define('DB_PASS', 'mypass');


//$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
//$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
require 'functions.php';
?>