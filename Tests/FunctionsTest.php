<?php

require __DIR__.'/../functions.php';

use PHPUnit\Framework\TestCase;

final class FunctionsTest extends TestCase
{
    public function testEscFunctionReturnSanitizedString(): void
    {
        $string = '<strong>Hello World!</strong>';
        $actual = esc($string);
        $expected = '&lt;strong&gt;Hello World!&lt;/strong&gt;';
        $this->assertEquals($actual, $expected);
    }

    public function testEscFunctionLeaveCleanStringAlone(): void
    {
        $string = 'Hello World!';
        $actual = esc($string);
        $expected = 'Hello World!';
        $this->assertEquals($actual, $expected);
    }

    public function testEscFunctionleaveEmptyStringEmpty(): void
    {
        $string = '';
        $actual = esc($string);
        $expected = '';
        $this->assertEquals($actual, $expected);
    }

    public function testEscAttributeEntitizesQuotes(): void
    {
        $string = "first'name";
        $actual =esc_attr($string);
        $expected = "first&#039;name";
        $this->assertEquals($actual, $expected);
    }

}


