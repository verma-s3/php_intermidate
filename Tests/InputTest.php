<?php

require __DIR__.'/../public/autoloaded.php';

use PHPUnit\Framework\TestCase;
use App\Input;

final class InputTest extends TestCase
{

 	public function testInputClassCanBeInstantiatesd()
 	{
 		$i = new Input();
 		$this->assertInstanceOf(Input::class,$i);
 	}

    public function testInputReturnsRawPostArray()
    {
    	$_POST = array(
 			'title' => '<h1>our Title</h1>',
 			'content' => '<p>Lorem ipsum text for content</p>'
    	);


    	//instantiate our input
    	$i = new Input();

    	$post = $i->post();

    	$this->assertCount(2,$post);
    }

    public function testInputReturnedRawArrayContainsTags()
    {
    	$_POST = array(
 			'title' => '<h1>our Title</h1>',
 			'content' => '<p>Lorem ipsum text for content</p>'
    	);


    	//instantiate our input
    	$i = new Input();

    	$post = $i->post();
    	$actual  = $post['title'];
    	$expected = '<h1>our Title</h1>';
    	$this->assertEquals($actual, $expected);	
    }

    public function testInputReturnSanitizedArray()
    {
    	$_POST = array(
 			'title' => '<h1>our Title</h1>',
 			'content' => '<p>Lorem ipsum text for content</p>'
    	);
    	//instantiate our input
    	$i = new Input();

    	$post = $i->cleanPost();
    	$this->assertCount(2,$post);
    }

     public function testInputReturnedCleanArrayContainsEntities()
    {
    	$_POST = array(
 			'title' => '<h1>our Title</h1>',
 			'content' => '<p>Lorem ipsum text for content</p>'
    	);


    	//instantiate our input
    	$i = new Input();

    	$post = $i->cleanPost();
    	$actual  = $post['title'];
    	$expected = '&lt;h1&gt;our Title&lt;/h1&gt;';
    	$this->assertEquals($actual, $expected);	
    }

    public function testInputReturnedSingleFieldOnRequest()
    {
    	$_POST = array(
 			'title' => '<h1>our Title</h1>',
 			'content' => '<p>Lorem ipsum text for content</p>'
    	);


    	//instantiate our input
    	$i = new Input();

    	$actual = $i->post('title');
    	$expected = '<h1>our Title</h1>';
    	$this->assertEquals($actual, $expected);
    }

}