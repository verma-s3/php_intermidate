<?php

require __DIR__.'/../public/autoloaded.php';

use PHPUnit\Framework\TestCase;
use App\Validator;

final class ValidatorTest extends TestCase
{
    public function testValidatorCanBeInstantiated(): void
    {
        $v = new Validator;
        $this->assertInstanceOf(Validator::class, $v);
    }

    public function testRequiredMethodRetuensErrorOnEmptyField()
    {
    	$_POST = [];
    	$v = new Validator;
    	$v->required('phone');
    	$errors = $v->getErrors();
    	$this->assertArrayHasKey('phone',$errors);
    }

    public function testRequiredMethodRetuensNoErrorVlaueExists()
    {
    	$_POST = ['phone' => '123-123-1256'];
    	$v = new Validator;
    	$v->required('phone');
    	$errors = $v->getErrors();
    	$this->assertEmpty($errors);
    }

    public function testRequiredMethodRetuensErrorOnFieldWithSpace()
    {
    	$_POST = ['phone' => ' '];
    	$v = new Validator;
    	$v->required('phone');
    	$errors = $v->getErrors();
    	$this->assertArrayHasKey('phone', $errors);
    }
    
    // no errors with valid phone number
    public function testRequiredMethodRetuensNoErrors()
    {
    	$_POST = [];
    	$_POST = ['phone' => '123-125-1256'];
    	$v = new Validator;
    	$v->phone('phone');
    	$errors = $v->getErrors();
    	$this->assertEmpty($errors);
    }

    // without dashes
    public function testRequiredMethodRetuensNoErrorsHasDashes()
    {
    	$_POST = [];
    	$_POST = ['phone' => '1231251256'];
    	$v = new Validator;
    	$v->phone('phone');
    	$errors = $v->getErrors();
    	$this->assertEmpty($errors);
    }

    // with invalid dashes
    public function testPhoneMethodRetuensErrorsHasInvalidDashes()
    {
    	$_POST = [];
    	$_POST = ['phone' => '1256'];
    	$v = new Validator;
    	$v->phone('phone');
    	$errors = $v->getErrors();
    	print_r($errors);
    	$this->assertArrayHasKey('phone',$errors);
    }

    // long value
    public function testRequiredMethodRetuensErrorsHasLoooongNumbers()
    {
    	$_POST = ['phone' => '1231251256156566'];
    	$v = new Validator;
    	$v->phone('phone');
    	$errors = $v->getErrors();
    	$this->assertArrayHasKey('phone',$errors);
    }
}