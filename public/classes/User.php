<?php    
    class User
    {
        /**
         * user' name
         * @var string
         */
        public $name;

        /**
         * user'email
         * @var string
         */
        public $email;

        /**
         * user's age
         * @var string
         */
        public $age;

        /**
         * user's password
         * @var string 
         */
        private $password;

        // magic function
        public function __construct($name, $email, $age, $password)
        {
            //var_dump('USER HAS BEEN CALLED');
            $this->name = $name;
            $this->email = $email;
            $this->age = $age;
            $this->password = $password;
        }

        // public function __destruct()
        // {
        //     var_dump('USER HAS BEEN CANCELLED');
        // }

        /**
         * set password
         * @param string $password
         */
        public function setPassword($password)
        {
            $this->password = $password;
        }

        /**
         * Get user's password
         * @return String password
         */
        public function getPassword()
        {
        	return $this->password;
        }
    }

?>