<?php

class Instructor extends User
{
  // instructor has all og user's public and protected properties and methods as well
  
  /**
   * Program in which instructor teaches
   * @var String
   */
  protected $program;

  public function __construct($name, $email, $age, $password, $program)
  {
  	$this->program = $program;
  	parent::__construct($name, $email, $age, $password);
  }
}

?>