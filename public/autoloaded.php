<?php 


// spl_autoload_register(function(){
//   code to locate and require class file.
// });

//  \App\Validator
//  
// __DIR__.'/sony_app/'.$class.'.php'


/**
 * Accept class name, convert to both path, require file
 * @param  string
 */

spl_autoload_register(function ($class)
{  
  	//Project base namespaces
  	$prefix = "App\\";

  	// base directory where my classes reside
  	$base_dir = __DIR__.'/sony_app/';

  	// get the length of prefix
  	$len = strlen($prefix);
    
    // echo strncmp($prefix, $class, $len);
    // die;

  	// test that class name passed in is using the prefix
  	if(strncmp($prefix, $class, $len)!==0){
  		return;
  	}
    
    // Get the class minus the
  	$sub_class = substr($class, $len);

  	//echo $sub_class;
    
    // ALternative way
    // 		way1 to write this with ltrim
  	//$sub_class = ltrim($class, 'App\\');
  	//      way 2 to write this with ltrim
    //$sub_class = ltrim($class, $prefix);
  	//echo $sub_class;
    $sub_class= str_replace('\\', '/', $sub_class);
    // another way
    //$sub_class= str_replace('\\', DIRECTORY_SEPRETOR, $sub_class);
  	$file = $base_dir.$sub_class.'.php';
  	echo $file;

  	if(file_exists($file)){
  		require $file;
  	}

  	//echo "done";

});


// //testing our function here
// autoload1('App\\Validator');

// $v = new App\Validator();

// var_dump($v);