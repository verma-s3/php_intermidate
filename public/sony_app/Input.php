<?php 

namespace App;

class Input
{
	protected $post;
	public function __construct()
	{
		$this->post = $_POST;
	}

	public function post($field=null)
	{
		if(is_null($field)){
			return $this->post;
		}
		elseif(isset($this->post[$field])){
			return $this->post[$field];
		}
		else{
			return false;
		}
		
	}

	public function cleanPost()
	{
		foreach($this->post as $key => $value){
   			$clean[$key] = htmlentities($value, ENT_QUOTES,"UTF-8");
		}
		return $clean;
	}
}