<?php
require __DIR__.'/../../config.php';
require __DIR__.'/../classes/Validator.php';

$v = new Validator();

$title = 'Register';

$error=[];
if('POST'== $_SERVER['REQUEST_METHOD']){
  // validate field
  $v->required('phone');
  $v->required('name');
  $v->required('email');

  $v->phone('phone');

  //count errors
  if(count($v->errors())===0){
  	var_dump('SUCCESS');
  	die;
  }
}// end test fro post

$errors = $v->errors();

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title><?=esc($title)?></title>
</head>
<body>
	<h1><?=esc($title)?></h1>
  <?php include __DIR__ . '/../inc/errors.inc.php'; ?>
	<form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
		<p>
			<label for="name">Name</label>
            <input type="text" id="name" name="name" value="<?=clean('name')?>"/>
		</p>
		<p>
			<label for="email">Email</label>
            <input type="text"  id="email" name="email" value="<?=clean('email')?>"/>
		</p>
		<p>
			<label for="phone">Phone</label>
            <input type="text"  id="phone" name="phone" value="<?=clean('phone')?>"/>
		</p>
		<button type="submit">Submit</button>
	</form>
</body>
</html>