<?php

// Regular expression basics


$pattern = '/cat/';
$string = "i am cat lover";

// 'cat' == nmatch 
// 'dogcat' == match

// in php we use preg_match (post)


if(preg_match($pattern, $string)){
	echo "<p>we have a match!</p>";
}
else{
	echo "<p>No match!</p>";
}

//preg_match has 3 potential return values
//
// 0 -- no match
// 1 -- match
// false -- error
// 
// how do I match exactly 'cat'
// 

$pattern ='/^cat$/';
$pattern2 = '/^cat$'; 
$string1 = 'I am a cat';
$string2 = "cat";

//should return 0 -- no match
var_dump(preg_match($pattern, $string1));

//should return 1 -- match
var_dump(preg_match($pattern, $string2));

//should return false -- error in pattern
var_dump(preg_match($pattern2, $string1));

//should return false -- error in pattern
var_dump(preg_match($pattern2, $string2));

// review of RegEx

$pattern = '/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})/';

$num1 = '204-222-1738';

//should return 1...... matching number
//careta an array in the current scope names $matches
//that contains our capture groups
var_dump(preg_match($pattern, $num1, $matches)); 

var_dump($matches);

// Note... outer group should be avilable in array before inner groups
// Eaxmple quiz questions 