<?php

// OBject oriented PHP
// 
// Javascript
// ls has prototype inheritance
// 
// PHP
// php has classical inheritance
// 		inheritance
// 		interface
// 		abstract classes
// 		
// 		
//what is class?
//A CLASS is a blueprint for creating objects.
//we can create many objects from the same class.
//
//
//Normally, in PHP, each class is stroed in its own file.
//with a filename that exactly matches the class name
//
//
//this class would be stored in a file names User.php
//
//Uppercasse camelcase
//
class User
{
    /**
     * user' name
     * @var string
     */
    public $name;

    /**
     * user'email
     * @var string
     */
    public $email;

    /**
     * user's age
     * @var string
     */
    public $age;

    /**
     * user's password
     * @var string 
     */
    private $password;

    /**
     * set password
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get user's password
     * @return String password
     */
    public function getPassword()
    {
    	return $this->password;
    }
}

$user1 = new User();

var_dump($user1);

// when we assign new property to an object from outside that object, they are by default 'public'... visible everywhere.
$user1->name = 'sonia';
$user1->email = 'verma';
$user1->age = 22;
$user1->setPassword(md5('mypass'));

var_dump($user1);

// properties (and methods) can be....
// public ... visible inside and outside the object.
// protected .. visible inside an object, and child object (inheritance)
// private .. visible ONLY withoh the object in which they are declared