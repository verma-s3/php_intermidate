<?php 
require_once __DIR__.'/../../config.php';
$string = "   dave     ";


echo "=>".$string.'<=';
echo "<br />\n";

//simple trim
echo"=>".trim($string)."<=";
echo "<br />\n";

// left trim
echo"=>".ltrim($string)."<=";
echo "<br />\n";

// right trim 
echo"=>".rtrim($string)."<=";
echo "<br />\n";

// removing other characters
// 
$names = array('Bob', 'sonia','iqbal','sunny');

// loop through names
// concate each name to the string
// append a , and space

$names_string = '';
foreach($names as $key){
   $names_string.=$key.', ';
}

echo $names_string;
echo "<br />\n";

$names_string = rtrim($names_string,', ');

echo $names_string;
echo "<br />\n";

//create array og mock DB results for a user table associative array. firld name as key, and appropriate value

$results = array(
  'user_id' => 3,
  'first_name' => 'Dave',
  'last_name' => 'Johns',
  'email' => 'example@gmail.com',
  'street' => '123 Any Street',
  'age' => 22
);

echo "<p>
        <strong>First name:</strong> {$results['first_name']}<br/>
        <strong>Last name:</strong> {$results['last_name']}<br/>
        <strong>Email:</strong> {$results['email']}<br/>
        <strong>Street:</strong> {$results['street']}<br/>
        <strong>Age:</strong> {$results['age']}<br/>
      </p>";

// we will create a function called label() 
// label() accepts a string 
// 
echo "<p>\n";
foreach($results as $key => $value){
	// doesnot show user id in the display data 
	if($key !== 'user_id'){
		$label = label($key);
		echo "<strong>$label</strong>: $value<br />";		
	}
}

echo "</p>\n";

// alternative way
echo "<p>\n";
foreach($results as $key => $value){
	echo "<strong>".label($key)."</strong>: $value<br />";
}

echo "</p>\n";

